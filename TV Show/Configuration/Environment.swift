//
//  Environment.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation
public enum Environment {
  // MARK: - Keys
  enum Keys {
    enum Plist {
      static let rootURL = "ROOT_URL"
      static let apiKey = "API_KEY"
      static let appBundleId = "APP_BUNDLE_ID"
      static let rootImagesURL = "ROOT_Images_URL"
    }
  }

  // MARK: - Plist
  private static let infoDictionary: [String: Any] = {
    guard let dict = Bundle.main.infoDictionary else {
      fatalError("Plist file not found")
    }
    return dict
  }()

  // MARK: - Plist values
  static let rootURL: URL = {
    guard let rootURLstring = Environment.infoDictionary[Keys.Plist.rootURL] as? String else {
      fatalError("Root URL not set in plist for this environment")
    }
    guard let url = URL(string: rootURLstring) else {
      fatalError("Root URL is invalid")
    }
    return url
  }()
    
    
  static let rootImagesURL: URL = {
        guard let rootURLstring = Environment.infoDictionary[Keys.Plist.rootImagesURL] as? String else {
            fatalError("Root Image URL not set in plist for this environment")
        }
        guard let url = URL(string: rootURLstring) else {
            fatalError("Root Image URL is invalid")
        }
        return url
    }()

  static let apiKey: String = {
    guard let apiKey = Environment.infoDictionary[Keys.Plist.apiKey] as? String else {
      fatalError("API Key not set in plist for this environment")
    }
    return apiKey
  }()
    
  static let appBundleId: String = {
    guard let appBundleId = Environment.infoDictionary[Keys.Plist.appBundleId] as? String else {
      fatalError("APP BUNDLE ID  not set in plist for this environment")
    }
    return appBundleId
  }()
}
