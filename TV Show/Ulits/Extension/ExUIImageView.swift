//
//  ExUIImageView.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Kingfisher

extension UIImageView {
    func setImage(with urlString: String){
        guard let url = URL.init(string: urlString) else {
            return
        }
        let resource = ImageResource(downloadURL: url, cacheKey: urlString)
        var kf = self.kf
        kf.indicatorType = .activity
        let defImage = #imageLiteral(resourceName: "appstore")
        self.kf.setImage(with: resource, placeholder: defImage)
    }
}

