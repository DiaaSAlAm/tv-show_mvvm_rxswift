//
//  ExArray.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
