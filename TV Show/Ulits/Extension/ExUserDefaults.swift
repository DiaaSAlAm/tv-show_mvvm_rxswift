//
//  ExUserDefaults.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit

typealias UD = UserDefaults

var prefs: UserDefaults {
    return UserDefaults.standard
}

extension UserDefaults {
    
    enum PrefKeys {
        static let recentSearchText = "recentSearchText"
        
    }
    
    func set<T>(encodable object: T?, forKey key: String) where T: Encodable {
        guard let object = object else {
            prefs.set(nil, forKey: key)
            return
        }
        if let encoded = try? JSONEncoder().encode(object) {
            prefs.set(encoded, forKey: key)
        }
    }
    
    func decodable<T>(forKey key: String, of type: T.Type) -> T? where T: Decodable {
        guard let jsonData = prefs.data(forKey: key),
            let object = try? JSONDecoder().decode(type, from: jsonData) else {
                return nil
        }
        return object
    }
}
