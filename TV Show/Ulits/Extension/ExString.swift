//
//  ExString.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit

extension String {
    
    func isEmptyOrWhitespace() -> Bool { //Check if text is empty or with white Soaces "    "
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
    
    var localized : String {
        return NSLocalizedString(self, comment: "")
    }
    // testTF.text = "en".localized == "en" ? somtext[en] ? somtext[ar]
}
