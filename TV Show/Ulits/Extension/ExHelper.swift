//
//  ExHelper.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//
 
 
import Foundation

public class Helper {
    
    /// Singletone instance
    public static let instance: Helper = Helper()
    
    // singletone
    private init(){}
    
    enum controllerID: String{
        case loginVC = "LoginVC"
    }
    
    enum storyboardName: String{
        case main = "Main"
    }
    
    enum cells: String {
        case tvShowsCell = "TVShowsCell"
        case networkCell = "NetworkCell"
        case seasonsCell = "SeasonsCell"
        case recommendationsCell = "RecommendationsCell"
        case favouritesTVShowsCell = "FavouritesTVShowsCell"
    }
    
   var recentSearchText : [String] {
        get {
            return prefs.stringArray(forKey: UD.PrefKeys.recentSearchText) ?? []
        }
        set {
            prefs.set(newValue, forKey: UD.PrefKeys.recentSearchText)
        }
    }
    
}


