//
//  TVShowsNetwork.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Alamofire

enum TVShowsNetwork {
    case getdiscoverTV
    case getTVDetails(tvId: Int)
    case rateing(tvId: Int, guestSessionId: String, value: Int)
    case createGuestSession
    case getSimilarTVShow(tvId: Int)
}

extension TVShowsNetwork: TargetType {
    
    var baseURL: String {
        return Environment.rootURL.absoluteString
    }
    
    
    var path: String {
        switch self {
        case .getdiscoverTV:
            return "discover/tv"
        case .getTVDetails(let tvId):
            return "tv/\(tvId)"
        case .rateing(let tvId, _, _):
            return "tv/\(tvId)/rating"
        case .createGuestSession:
            return "authentication/guest_session/new"
        case .getSimilarTVShow(let tvId):
            return "tv/\(tvId)/similar"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .rateing:
            return .post
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .rateing:
            return ["Content-Type": "application/json"]
        default:
            return [:]
        }
    }
    
    var task: Task {
        switch self {
        case .getdiscoverTV:
            return .requestParametersURLEncoding(parameters: ["api_key": Environment.apiKey])
        case .getTVDetails(_):
            return .requestParametersURLEncoding(parameters: ["api_key": Environment.apiKey])
        case .rateing(_,let guestSessionId,let value):
            return .requestQueryStringWithRequestBody(queryStringParameters: ["api_key": Environment.apiKey, "guest_session_id": guestSessionId], requestBodyParameters: ["value": value])
        case .createGuestSession:
            return .requestParametersURLEncoding(parameters: ["api_key": Environment.apiKey])
        case .getSimilarTVShow(_):
            return .requestParametersURLEncoding(parameters: ["api_key": Environment.apiKey])
        }
    }
}
