//
//  TVShowsListAPI.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation
 
protocol TVShowsAPIProtocol {
    
    func getSimilarTVShow(tvId: Int,completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void)
}


class TVShowsAPI: BaseAPI<TVShowsNetwork>, TVShowsAPIProtocol {
    
    func getSimilarTVShow(tvId: Int, completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void) {
        self.fetchData(target: .getSimilarTVShow(tvId: tvId), responseClass: ListOfTVShowsModel.self, completion: completion)
    }
}
