//
//  ListOfTVShowsModel.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation

struct ListOfTVShowsModel : Codable {
    let totalResults : Int?
    let page : Int?
    let totalPages : Int?
    let results : [ResultsModel]?
    var success: Bool?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results = "results"
        case success , page , message
    }

}

class ResultsModel :Codable {
    let first_air_date : String?
    let poster_path : String?
    let vote_average : Double?
    let overview : String?
    let popularity : Double?
    let original_name : String?
    let origin_country : [String]?
    let vote_count : Int?
    let backdrop_path : String?
    let name : String?
    let genre_ids : [Int]?
    let id : Int?
    let original_language : String?
    var isFavourite: Bool?

    enum CodingKeys: String, CodingKey {

        case first_air_date = "first_air_date"
        case poster_path = "poster_path"
        case vote_average = "vote_average"
        case overview = "overview"
        case popularity = "popularity"
        case original_name = "original_name"
        case origin_country = "origin_country"
        case vote_count = "vote_count"
        case backdrop_path = "backdrop_path"
        case name = "name"
        case genre_ids = "genre_ids"
        case id = "id"
        case original_language = "original_language"
    }
}
 
