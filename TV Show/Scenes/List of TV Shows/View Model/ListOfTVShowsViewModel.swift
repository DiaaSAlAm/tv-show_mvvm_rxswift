//
//  ListOfTVShowsViewModel.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation
import RxSwift
import RxCocoa

class ListOfTVShowsViewModel {
    
    //MARK: - Properties
    var loadingBehavior = BehaviorRelay<Bool>(value: false)
    var messageBehavior = BehaviorRelay<String>(value: "")
    private var isCollectionViewHiddenBehavior = BehaviorRelay<Bool>(value: false)
    private var listOfTVShowResultSubject = PublishSubject<[ResultsModel]>()
    private let tvShowsListNetwork: TVShowsAPIProtocol = TVShowsAPI()
    
    var loadingObservable: Observable<Bool> {
        return loadingBehavior.asObservable()
    }
    
    var messageObservable: Observable<String> {
        return messageBehavior.asObservable()
    }
    
    var isCollectionViewHiddenObservable: Observable<Bool> {
        return isCollectionViewHiddenBehavior.asObservable()
    }
    
    var listOfTVShowResultObservable: Observable<[ResultsModel]> {
        return listOfTVShowResultSubject
    }
    
    
    //MARK: API Service
    func getdiscoverTV() {
        loadingBehavior.accept(true)
        tvShowsListNetwork.getdiscoverTV { [weak self] (result) in
            guard let self = self else {return}
            defer {self.loadingBehavior.accept(false)}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.isCollectionViewHiddenBehavior.accept(false)
                    self.messageBehavior.accept(response?.message ?? ConstantsMessage.genericErrorMessage)
                default:
                    guard response?.results?.count ?? 0 > 0 else {
                        self.isCollectionViewHiddenBehavior.accept(true)
                        return
                    }
                    self.listOfTVShowResultSubject.onNext(response?.results ?? [])
                    self.isCollectionViewHiddenBehavior.accept(false)
                    
                    
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.isCollectionViewHiddenBehavior.accept(false)
                self.messageBehavior.accept(ConstantsMessage.genericErrorMessage)
            }
        }
    }
}
