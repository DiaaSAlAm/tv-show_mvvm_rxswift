//
//  ListOfTVShowsVC.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit
import RxSwift
import RxCocoa

class ListOfTVShowsVC: UIViewController {

     //MARK: - IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    private let cellIdentifier = Helper.cells.tvShowsCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .large)
    private let fadeView: UIView = UIView()
    let listOfTVShowsViewModel = ListOfTVShowsViewModel()
    let disposeBag = DisposeBag()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
}

//MARK: Extension Setup View 
extension ListOfTVShowsVC {
    
    fileprivate func setupUI() {
        registerCollectionView()
        bindToHiddenCollection()
        subscribeToLoading()
        subscribeToResponse()
        subscribeToShowMessage()
        subscribeToBranchSelection()
        getdiscoverTV()
    }
    
    //MARK: - Register  CollectionView Cell
    private func registerCollectionView(){
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - Start Animating Activity
    private func startAnimatingActivityView() {
        fadeView.frame = self.view.frame
        fadeView.backgroundColor = .black
        fadeView.alpha = 0.4
        self.view.addSubview(fadeView)

        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
    }
    
    //MARK: - Stop Animating Activity
    private func stopAnimatingActivityView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.fadeView.removeFromSuperview()
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    private func bindToHiddenCollection() {
        listOfTVShowsViewModel.isCollectionViewHiddenObservable.bind(to: self.collectionView.rx.isHidden).disposed(by: disposeBag)
    }
    
    private func subscribeToLoading() {
        listOfTVShowsViewModel.loadingBehavior.subscribe(onNext: { [weak self] (isLoading) in
            guard let self = self else {return}
            if isLoading {
                self.startAnimatingActivityView()
            } else {
                self.stopAnimatingActivityView()
            }
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToShowMessage() {
        listOfTVShowsViewModel.messageBehavior.subscribe(onNext: { [weak self] (message) in
            guard let self = self else {return}
            guard message != "" else {return}
            ToastManager.shared.showMessage(message: message, view: self.view, backgroundColor: .red){
                self.addTryAgainButton()
            }
          
        }).disposed(by: disposeBag)
    }
    
    func addTryAgainButton() {
        let button = RoundedButton()
        button.cornerRadius = 10
        button.frame.size.width = 100
        button.frame.size.height = 30
        button.center = self.view.center
        button.backgroundColor = .black
        button.setTitle("Try again", for: .normal)
        button.addTarget(self, action: #selector(self.TryAgainButton), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func TryAgainButton(sender: UIButton!) {
        sender.removeFromSuperview()
        getdiscoverTV() 
    }
    
    private func subscribeToResponse() {
        self.listOfTVShowsViewModel.listOfTVShowResultObservable
            .bind(to: self.collectionView
            .rx
            .items(cellIdentifier: cellIdentifier, cellType: TVShowsCell.self)) { row, results, cell in
                cell.configerCell(resultsModel: results)
            }.disposed(by: disposeBag)
    }
     
    
    private func subscribeToBranchSelection() {
        Observable
            .zip(collectionView.rx.itemSelected, collectionView.rx.modelSelected(ResultsModel.self))
            .bind {   selectedIndex, results in
                
                print(selectedIndex, results.name ?? "")
        }
        .disposed(by: disposeBag)
    }
    
    func getdiscoverTV() {
        listOfTVShowsViewModel.getdiscoverTV()
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension ListOfTVShowsVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width
        return CGSize(width: (width / 2) , height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell horizonatally
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell vertically
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        // give space top left bottom and right for cells
        return UIEdgeInsets(top: 10, left: 0 , bottom: 10, right: 0)
    }
}
