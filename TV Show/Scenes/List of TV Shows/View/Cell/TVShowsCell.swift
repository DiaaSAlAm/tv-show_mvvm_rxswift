//
//  TVShowsCell.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit

class TVShowsCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var voteAverage: UILabel!
    @IBOutlet weak var firstAirDate: UILabel!
    @IBOutlet weak var posterPathImage: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    
    //MARK: - Properties
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        posterPathImage.layer.cornerRadius = 8
    }
    
    func configerCell(resultsModel: ResultsModel){
        firstAirDate.text = resultsModel.first_air_date ?? ""
        name.text = resultsModel.name ?? ""
        voteAverage.text = "\(resultsModel.vote_average ?? 0)"
        let path = (Environment.rootImagesURL.absoluteString) + (resultsModel.poster_path ?? "")
        posterPathImage.setImage(with: path)
    }

}
