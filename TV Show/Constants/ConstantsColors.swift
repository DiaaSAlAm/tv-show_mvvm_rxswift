//
//  ConstantsColors.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit

struct ConstantsColors {
    static let mainColor = UIColor(named: "mainColor")
    static let backgroundColor = UIColor(named: "backgroundColor")
}

