//
//  AppStarter.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit
//import IQKeyboardManagerSwift

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    
    private init() {}
    
    func start(window: UIWindow?) {
        AppTheme.apply()
//        setupKeyboardConfig()
        setRootViewController(window: window)
    }
    
    
//    private func setupKeyboardConfig() {
//        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
//    }
    
    private func setRootViewController(window: UIWindow?) {
        let storyboard = UIStoryboard.init(name: Helper.storyboardName.main.rawValue, bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
}
