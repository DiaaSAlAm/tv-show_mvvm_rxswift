//
//  AppTheme.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import UIKit

/// Apply application theme and colors
class AppTheme {
    
    static func apply() {
        navigationBarStyle()
        searchBarStyle()
    }
    
    private static func navigationBarStyle() {
        let backImage = UIImage(named: "Back")
        UINavigationBar.appearance().backIndicatorImage =  backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0, green: 0.8117647059, blue: 0.9568627451, alpha: 1)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    private static  func searchBarStyle() {
        let searchTextField = UITextField.appearance(
            whenContainedInInstancesOf: ([UISearchBar.self])
        )
        searchTextField.backgroundColor = .white
    }
}
