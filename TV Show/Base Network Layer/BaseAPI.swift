//
//  BaseAPI.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Foundation
import Alamofire

class BaseAPI<T: TargetType> {
    
    func fetchData<M: Decodable>(target: T, responseClass: M.Type, completion:@escaping (Result<M?, NSError>) -> Void) {
        let method = Alamofire.HTTPMethod(rawValue: target.method.rawValue)
        let headers = target.headers
        let parameters = buildParams(task: target.task)
        var urlComponent = URLComponents(string: target.baseURL + target.path)!
        switch target.task {
        case .requestParametersURLEncoding, .requestQueryStringWithRequestBody:
            let queryItems = parameters.0.map  { URLQueryItem(name: $0.key, value: $0.value as? String) }
            urlComponent.queryItems = queryItems
        default:
            break
        }
        
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        
        switch target.task {
        case .requestQueryStringWithRequestBody:
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters.2)
        case .requestParametersJSONEncoding:
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters.0)
        default:
            break
        }
        
        AF.request(request).responseJSON { (response) in
            
            print("response $$$$$$$$$$$$$$$$$$$$$$", response)
            print("parameters", parameters.0)
            
            guard let statusCode = response.response?.statusCode else {
                // ADD Custom Error
                let error = NSError(domain: target.baseURL, code: 0, userInfo: [NSLocalizedDescriptionKey: ConstantsMessage.genericErrorMessage])
                completion(.failure(error))
                return
            }
            if 200 ..< 401 ~= statusCode { // 200 reflect success response
                // Successful request
                guard let jsonResponse = try? response.result.get() else {
                    // ADD Custom Error
                    let error = NSError(domain: target.baseURL, code: 0, userInfo: [NSLocalizedDescriptionKey: ConstantsMessage.genericErrorMessage])
                    completion(.failure(error))
                    return
                }
                guard let theJSONData = try? JSONSerialization.data(withJSONObject: jsonResponse, options: []) else {
                    // ADD Custom Error
                    let error = NSError(domain: target.baseURL, code: 0, userInfo: [NSLocalizedDescriptionKey: ConstantsMessage.genericErrorMessage])
                    completion(.failure(error))
                    return
                }
                guard let responseObj = try? JSONDecoder().decode(M.self, from: theJSONData) else {
                    // ADD Custom Error
                    let error = NSError(domain: target.baseURL, code: 0, userInfo: [NSLocalizedDescriptionKey: ConstantsMessage.genericErrorMessage])
                    completion(.failure(error))
                    return
                }
                completion(.success(responseObj))
            } else {
                // ADD custom error base on status code 404 / 401 /
                // Error Parsing for the error message from the BE
                let message = "Error Message Parsed From BE"
                let error = NSError(domain: target.baseURL, code: statusCode, userInfo: [NSLocalizedDescriptionKey: message])
                completion(.failure(error))
            }
        }
    }
     
    
    private func buildParams(task: Task) -> ([String:Any], ParameterEncoding?, [String:Any]) {
        
        switch task {
        case .requestPlain:
             return ([:], URLEncoding.default, [:])
        case .requestParametersURLEncoding(let parameters):
            return (parameters, URLEncoding.default, [:])
        case .requestParametersJSONEncoding(let parameters):
            return (parameters, JSONEncoding.default, [:])
        case .requestQueryStringWithRequestBody(let queryStringParameters, let requestBodyParameters):
            return (queryStringParameters, nil, requestBodyParameters)
        }
    }
}
