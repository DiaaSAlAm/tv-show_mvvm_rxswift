//
//  TargetType.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

import Alamofire
import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

enum Task {
    
    /// A request with no additional data.
    case requestPlain
    
    /// A requests body set with encoded parameters.
//    case requestParameters(parameters: [String: Any], encoding: ParameterEncoding)
    
    case requestParametersURLEncoding(parameters: [String: Any])
    
    case requestParametersJSONEncoding(parameters: [String: Any])
    
    case requestQueryStringWithRequestBody(queryStringParameters: [String: Any], requestBodyParameters: [String: Any])
    
}

protocol TargetType {
    
    /// The target's base `URL`.
    var baseURL: String { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: HTTPMethod { get }
    
    /// The type of HTTP task to be performed.
    var task: Task { get }
    
    /// The headers to be used in the request.
    var headers: [String: String]? { get }
}
