//
//  BaseResponse.swift
//  TV Show
//
//  Created by mac on 05/01/2021.
//

class BaseResponse<T: Codable>: Codable {
    var success: Bool?
    var data: T?
    var message: String?
    var statusCode: Int?
    
    enum CodingKeys: String, CodingKey {
        case success
        case message = "status_message"
        case statusCode = "status_code"
    }
}
